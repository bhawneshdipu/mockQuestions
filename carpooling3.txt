

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Main {
    
    public static void main(String[] args) throws IOException {
        String menu = "Menu:\n 1) Valid Car registration Number\n 2) Convert Car registration Number\n "
                + "3) Valid Driving License";
        
        //TN-07-AS-1273
        System.out.println(menu);
        System.out.println("Enter choice");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int x = Integer.parseInt(br.readLine());
        String regnum = "",input="";
        switch(x) {
            case 1: {
                System.out.println("car registration number");
                regnum = br.readLine();
                validateRegNum(regnum);
                break;
            }
            case 2: {
                System.out.println("car registration number");
                regnum = br.readLine();
                convertRegNum(regnum);
                break;
            }
            case 3: {
                System.out.println("driving license issue date");
                input = br.readLine();
                try {
					validateDrivingLicense(input);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                break;
            }
            
        }
        
        
        
    }
    //String comp="(regnum.charAt(0)>64 && regnum.charAt(0)<91 && regnum.charAt(1)>64 && regnum.charAt(1)<91 && regnum.charAt(2)=='-' && regnum.charAt(2)='-'";
    public static void validateRegNum(String reg) {
    	if(reg.length()==13 && reg.charAt(2)=='-' && reg.charAt(5)=='-' && reg.charAt(8)=='-' &&
	    		  (reg.charAt(0) >= 'A' && reg.charAt(0) <= 'Z') &&  (reg.charAt(1) >= 'A' && reg.charAt(1) <= 'Z')
	    		  && (reg.charAt(3) >= '0' && reg.charAt(3) <= '9') &&  (reg.charAt(4) >= '0' && reg.charAt(4) <= '9')
	   		  && (reg.charAt(6) >= 'A' && reg.charAt(6) <= 'Z') &&  (reg.charAt(7) >= 'A' && reg.charAt(7) <= 'Z')			    		  && (reg.charAt(9) >= '0' && reg.charAt(9) <= '9') && (reg.charAt(10) >= '0' && reg.charAt(10) <= '9')
	    		  && (reg.charAt(11) >= '0' && reg.charAt(11) <= '9') && (reg.charAt(12) >= '0' && reg.charAt(12) <= '9')){
    		System.out.println(reg+" is Valid");
    	}
    	else{
    		System.out.println(reg+" is Invalid");
    	}
    	System.exit(0);
    }

    public static void convertRegNum(String reg) {
    	if(reg.length()==13 ){
    		String s1=reg.replace(Character.toString(reg.charAt(2)), "-");
    		String s2=s1.replace(Character.toString(s1.charAt(5)), "-");
    		String s3=s2.replace(Character.toString(s2.charAt(8)), "-");
    		System.out.println(s3);
    }
    	
    	System.exit(0);
    }
    
    public static void validateDrivingLicense(String inp) throws ParseException {
    	SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
    	Date d1=sdf.parse(inp);
    	String s1="15-06-2017";
    	
    	Date exp=sdf.parse(s1);
    	long diffInMilliSec=exp.getTime()-d1.getTime();
    	long years =  (diffInMilliSec / (1000l * 60 * 60 * 24 * 365));
    	//System.out.println(years);
    	
 	if(years<10){
  		System.out.println(years+" years old license - valid");    		  	}
   	else{
  		System.out.println(years+" years old license - expired");
   	}
   	
   	System.exit(0);
    }
    
}

